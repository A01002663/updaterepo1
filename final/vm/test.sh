vboxmanage startvm mail
until [[ $ssh1 == 0 ]]
do
        echo "Attempting SSH Connection... Retrying." 
        sleep 15
        scp -rp ../updaterepo/ $user@10.16.1.100:/home/$user/ > /dev/null 2>&1
        ssh1=$?
done
echo "SSH Complete."
echo "Executing scripts now..."
ssh $user@10.16.255.1 'source updaterepo/sourcelist.sh; sudo bash -x updaterepo/mailexecute.sh'

