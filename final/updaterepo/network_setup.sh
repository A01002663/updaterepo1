
source updaterepo/sourcelist.sh
cat > /etc/sysconfig/network-scripts/ifcfg-${local_if} << EOF
BOOTPROTO=none
ONBOOT=yes
TYPE=Ethernet
NAME=$local_if
DEVICE=$local_if
IPADDR=10.16.${sid}.126
PREFIX=${local_cidr}
DEFROUTE=no
EOF
#cat > /etc/sysconfig/network-scripts/ifcfg-${wifi_if} << EOF
#DEVICE="${wifi_if}"
#BOOTPROTO=none
#TYPE="Wireless"
#ONBOOT=yes
#NM_CONTROLLED=no
#IPADDR=10.16.1.129
#PREFIX=25
#ESSID="NASP19_S${fsid}"
#CHANNEL="11"
#MODE="Master"
#RATE="Auto"

#EOF
