#!/bin/bash

systemctl start unbound > /dev/null 2>&1
systemctl enable unbound > /dev/null 2>&1
systemctl restart unbound > /dev/null 2>&1
systemctl start nsd > /dev/null 2>&1
systemctl enable nsd > /dev/null 2>&1
systemctl restart nsd > /dev/null 2>&1
systemctl start network > /dev/null 2>&1
systemctl enable network > /dev/null 2>&1
systemctl restart network > /dev/null 2>&1
systemctl start dhcpd > /dev/null 2>&1
systemctl enable dhcpd > /dev/null 2>&1
systemctl restart dhcpd > /dev/null 2>&1
systemctl start zebra > /dev/null 2>&1
systemctl enable zebra > /dev/null 2>&1
systemctl restart zebra > /dev/null 2>&1
systemctl start ospfd > /dev/null 2>&1
systemctl enable ospfd > /dev/null 2>&1
systemctl restart ospfd > /dev/null 2>&1
systemctl start hostapd > /dev/null 2>&1
systemctl enable hostapd > /dev/null 2>&1
systemctl restart hostapd > /dev/null 2>&1


echo "All services restarted on `hostname`."
