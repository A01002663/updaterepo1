#!/bin/bash

source updaterepo/sourcelist.sh

type nsd > /dev/null 2>&1
nsd_avail=$?

if [[ $nsd_avail == 1 ]]; then
  echo "You do not have nsd installed. It is being installed and configured now."
  yum install -y nsd > /dev/null 2>&1
else
  echo "You have nsd installed. Configuring now."
fi



touch /etc/nsd/nsd.conf
cat > /etc/nsd/nsd.conf << NSDEOF
server:
	ip-address: 10.16.255.${sid}
	do-ip6: no
	verbosity: 10
	include: "/etc/nsd/server.d/*.conf"

include: "/etc/nsd/conf.d/*.conf"
remote-control:
	control-enable: yes

zone:
	name: "s${fsid}.as.learn"
	zonefile: "s${fsid}.as.learn.zone"

zone:
	name: "${sid}.16.10.in-addr.arpa"
	zonefile: "${sid}.16.10.in-addr.arpa.zone"

NSDEOF
touch /etc/nsd/${sid}.16.10.in-addr.arpa.zone
cat > /etc/nsd/${sid}.16.10.in-addr.arpa.zone << EOF
\$TTL 10s
${sid}.16.10.in-addr.arpa.    IN  SOA  rtr.${fsid}.as.learn. hi (
                        2016030101    ; serial number of Zone Record
                        1200s         ; refresh time
                        180s          ; retry time on failure
                        1d            ; expiration time
                        3600          ; cache time to live
                        )

;Name servers for this domain
${sid}.16.10.in-addr.arpa.    IN  NS  s${fsid}rtr.as.learn.

126.${sid}.16.10.in-addr.arpa.  IN  PTR  rtr.s${fsid}.as.learn.
100.${sid}.16.10.in-addr.arpa.  IN  PTR  mail.s${fsid}.as.learn.



EOF
cat > /etc/nsd/s${fsid}.as.learn.zone << EOF
\$TTL 10s                              ; 10 secs default TTL for zone
s${fsid}.as.learn.	IN	SOA	s${fsid}rtr.as.learn. hi (
                        2014022501    ; serial number of Zone Record
                        1200s         ; refresh time
                        180s          ; update retry time on failure
                        1d            ; expiration time (if the primary server does not reply then the server will stop replying to the clients)
                        3600          ; cache time to live (for this SOA record)
                        )

;Name servers for this domain
s${fsid}.as.learn.	IN	NS	s${fsid}rtr.as.learn.

mail.s${fsid}.as.learn.	IN	MX	10	mail

rtr.s${fsid}.as.learn.	IN	A	10.16.${sid}.126
mail.s${fsid}.as.learn.	IN	A	10.16.${sid}.100
mailserver	CNAME	mail.s${fsid}.as.learn.


EOF
sudo nsd-control-setup > /dev/null 2>&1



echo "DNS setup is running on `hostname`."
