#!/bin/bash
#Checks if DHCP is available. If not, it will install it.

source updaterepo/sourcelist.sh

type dhcpd > /dev/null 2>&1
dhcp_avail=$?

if [[ $dhcp_avail == 1 ]]; then
  echo "You do not have DHCP installed. It is being installed and configured now."
  yum install -y dhcp > /dev/null 2>&1
else
  echo "You have DHCP installed. Configuring now."
fi
#Removes existing config files and adds the new configured one.
rm -f /etc/dhcp/dhcpd.conf
touch /etc/dhcp/dhcpd.conf
cat > /etc/dhcp/dhcpd.conf << CONF
option domain-name "s${fsid}.as.learn";
        subnet 10.16.${sid}.0 netmask 255.255.255.128 {
        option routers 10.16.${sid}.126;
        option subnet-mask 255.255.255.128;
        option broadcast-address 10.16.${sid}.127;
        range 10.16.${sid}.100 10.16.${sid}.126;
}
# Static IP Assignment
host s${fsid}.MailServer {
        hardware ethernet ${mail_mac1};
        fixed-address 10.16.${sid}.100;
}

	subnet 10.16.${sid}.128 netmask 255.255.255.128 {
	option routers 10.16.${sid}.129;
	option subnet-mask 255.255.255.128;
	option broadcast-address 10.16.${sid}.255;
	option domain-name-servers 10.16.${sid}.126;
	range 10.16.${sid}.129 10.16.${sid}.254;
}

CONF

#Starts the services.
echo "DHCP is now running on `hostname`"
