#!/bin/bash

yum install -y telnet > /dev/null 2>&1

sed -i 's/#inet_interfaces = localhost/inet_interfaces = localhost/g' /etc/postfix/main.cf

sed -i 's/#home_mailbox = Maildir/home_mailbox = Maildir/g' /etc/postfix/main.cf

sed -i 's/#mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain/mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain/g' /etc/postfix/main.cf

systemctl restart postfix.service > /dev/null 2>&1

echo "Postfix configured on `hostname`."
