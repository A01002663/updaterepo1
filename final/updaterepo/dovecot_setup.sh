#!/bin/bash

yum install -y dovecot > /dev/null 2>&1
systemctl enable dovecot > /dev/null 2>&1

sed -i 's/#protocols = imap pop3 lmtp/protocols = imap lmtp/g' /etc/dovecot/dovecot.conf
sed -i 's/#   mail_location = maildir:~\/Maildir/mail_location = maildir:~\/Maildir/g' /etc/dovecot/conf.d/10-mail.conf

sed -i 's/#disable_plaintext_auth = yes/disable_plaintext_auth = no/g' /etc/dovecot/conf.d/10-auth.conf
systemctl restart dovecot > /dev/null 2>&1
echo "Dovecot is now running on `hostname`/"
