#!/bin/bash

source updaterepo/sourcelist.sh
yum -y update > /dev/null 2>&1
yum -y group install base > /dev/null 2>&1

yum -y install epel-release > /dev/null 2>&1
yum -y update > /dev/null 2>&1

yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git > /dev/null 2>&1

yum -y install kernel-devel kernel-headers dkms gcc gcc-c+ > /dev/null 2>&1



systemctl stop firewalld.service > /dev/null 2>&1
systemctl disable firewalld.service > /dev/null 2>&1

systemctl enable network.service > /dev/null 2>&1
systemctl start network.service > /dev/null 2>&1

systemctl enable sshd.service > /dev/null 2>&1
systemctl start sshd.service > /dev/null 2>&1

cat > /etc/sysconfig/network-scripts/ifcfg-$local_if << EOF
BOOTPROTO=none
ONBOOT=yes
TYPE=Ethernet
NAME=$local_if
DEVICE=$local_if
IPADDR=10.16.${sid}.126
PREFIX=$local_cidr
DEFROUTE=no
EOF
#cat > /etc/sysconfig/network-scripts/ifcfg-wlp0s11u2 << EOF
#DEVICE="${wifi_if}"
#BOOTPROTO=none
#TYPE="Wireless"
#ONBOOT=yes
#NM_CONTROLLED=no
#IPADDR=10.16.${sid}.129
#PREFIX=25
#ESSID="NASP19_S{$fsid}"
#CHANNEL="11"
#MODE="Master"
#RATE="Auto"

# EOF
echo "Network interface configuration is complete on `hostname`"
