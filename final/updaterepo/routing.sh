#!/bin/bash

source updaterepo/sourcelist.sh

type quagga
quagga_avail=$?

if [[ $quagga_avail == 1 ]]; then
  echo "You do not have quagga installed. It is being installed and configured now."
  yum install -y quagga > /dev/null 2>&1
else
  echo "You have quagga installed. Configuring now."
fi

rm -f /etc/quagga/ospfd.conf
touch /etc/quagga/ospfd.conf
chown quagga:quagga /etc/quagga/ospfd.conf

#Adds the required config
cat > /etc/quagga/ospfd.conf << EOF
hostname ospfd
password zebra
log stdout
interface $ext_if
interface $local_if
#interface wlps011u2
interface lo
router ospf
        ospf router-id 10.16.255.${sid}
        network 10.16.${sid}.0/25 area 0.0.0.0
	network 10.16.${sid}.128/25 area 0.0.0.0
        network 10.16.255.0/24 area 0.0.0.0
line vty
EOF
#Configure Services

echo -e "QUAGGA is configured on `hostname`.\n"

echo 1 > /proc/sys/net/ipv4/ip_forward
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
echo "IP Routing status: `cat /proc/sys/net/ipv4/ip_forward`"
echo "Routing is not working on `hostname`."
