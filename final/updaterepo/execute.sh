#!/bin/bash
source updaterepo/sourcelist.sh

updaterepo/./base_configuration.sh
updaterepo/./selinux_setup.sh
updaterepo/./network_setup.sh
#/home/$current/updaterepo/./wifi_if_setup.sh
updaterepo/./nsd_setup.sh
updaterepo/./unbound_setup.sh
updaterepo/./dhcpd_setup.sh
updaterepo/./routing.sh
#/home/$current/updaterepo/./hostapd_setup.sh
updaterepo/./servicerestart.sh
