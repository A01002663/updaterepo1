source updaterepo/sourcelist.sh
type hostapd
apd_avail=$?

if [[ $apd_avail == 1 ]]; then
  echo "You do not have DHCP installed. It is being installed and configured now."
  yum install -y hostapd > /dev/null 2>&1
else
  echo "You have HostAPD installed. It will be configured now."
fi


sudo dhclient wlp0s6u1 > /dev/null 2>&1
