#!/bin/bash

source updaterepo/sourcelist.sh

type unbound
unbound_avail=$?

if [[ $unbound_avail == 1 ]]; then
  echo "You do not have Unbound installed. It is being installed and configured now."
  yum install -y unbound > /dev/null 2>&1
else
  echo "You have Unbound installed. Configuring now."
fi


rm -rf /etc/unbound/unbound.conf
touch /etc/unbound/unbound.conf
cat > /etc/unbound/unbound.conf << EOF
server:
	interface: 10.16.${sid}.126
# 	interface: 10.16.${sid}.129
        port: 53
        do-ip4: yes
        do-ip6: no
        access-control: 3.0.0.0/0 refuse
        access-control: ::0/0 refuse
        access-control: 10.16.255.0/24 refuse
        access-control: 10.16.${sid}.128/25 allow
        access-control: 10.16.${sid}.0/25 allow
        chroot: ""
        username: "unbound"
        directory: "/etc/unbound"
        root-hints: "/etc/unbound/root.hints"
        pidfile: "/var/run/unbound/unbound.pid"
        prefetch: yes
        module-config: "iterator"
        include: /etc/unbound/local.d/*.conf

remote-control:
        control-enable: yes
        server-key-file: "/etc/unbound/unbound_server.key"
        server-cert-file: "/etc/unbound/unbound_server.pem"
        control-key-file: "/etc/unbound/unbound_control.key"
        control-cert-file: "/etc/unbound/unbound_control.pem"
        include: /etc/unbound/conf.d/*.conf

stub-zone:
        name: "s${fsid}.as.learn"
        stub-addr: 10.16.255.${sid}

stub-zone:
        name: "${sid}.16.10.in-addr.arpa"
        stub-addr: 10.16.255.${sid}

forward-zone:
	name: "learn"
	forward-addr: 142.232.221.253

forward-zone:
	name: "bcit.ca"
	forward-addr: 142.232.221.253

forward-zone:
	name: "htpbcit.ca"
	forward-addr: 142.232.221.253

EOF
cd ~
wget https://www.internic.net/domain/named.cache > /dev/null 2>&1
mv ~/named.cache /etc/unbound/
mv /etc/unbound/named.cache /etc/unbound/root.hints
cd -
echo "Unbound is now working on `hostname`."
