To run the final script, execute "bigredbutton.sh" located in the "vm" directory. The Kickstart files are supplied, but the ISOs will pull them from the FTP Server in the 142.232.221.0/24 network.

The wi-fi interface was excluded due to hardware issues.

DIRECTORIES:

activities: linux scripting acvities
final: final project
KS: kickstart files for reference

